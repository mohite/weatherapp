//
//  DataModel.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 20/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import Foundation



struct CoordModel:Decodable {
    var lon:Double!
    var lat:Double!
}

struct WeatherModel:Decodable {
    let id: Int
    let main:String
    let description:String
    let icon:String
}

//struct MainModel:Decodable {
//    let temp: Double
//    let pressure:Double
//    let humidity:Double
//    let temp_min:Double
//    let temp_max:Double
//}

struct MainModel: Decodable {
    let temp: Double
    let pressure:Double
    let humidity:Double
    let temp_min:Double
    let temp_max:Double
    
//    enum CodingKeys: String, CodingKey {
//        case currentTemp = "temp"
//        case pressure
//        case humidity
//        case minimumTemp  = "temp_min"
//        case maximumTemp = "temp_max"
//    }
}


struct WindModel:Decodable
{
    let speed:Double
    let deg:Double
}

struct CloudModel:Decodable
{
    let all:Int
}

struct SysModel:Decodable
{
    let type:Int
    let id:Int
    let message: Float
    let country :String
    let sunrise: Int
    let sunset:Int
}
struct DataModel:Decodable
{
    var coord:CoordModel!
    var weather: [WeatherModel]!
    var base: String!
    var main: MainModel!
    var visibility:Double!
    var cloud:CloudModel!
    var wind: WindModel!
    var dt: Int!
    var sys:SysModel!
    var id: Int!
    var name: String!
    var cod: Int!
}
struct getResults: Decodable
{
    var cnt: Int?
    var list:[DataModel]?
}


public func getStringFromUnixTimeStamp(dateStamp: Int)->(String)
{
    let date = NSDate(timeIntervalSince1970: TimeInterval(dateStamp))
    
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
    
    let dateString = dayTimePeriodFormatter.string(from: date as Date)
    return dateString
}
