//
//  WeatherAPI.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 24/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import Foundation
import Siesta
class WeatherAPI
{
    static let sharedInstance = WeatherAPI()

    private let service = Service(baseURL: "http://api.openweathermap.org/data/2.5", standardTransformers: [.text, .image])

    private init() {
        
        SiestaLog.Category.enabled = [.network, .pipeline, .observers]
        let jsonDecoder = JSONDecoder()
        
        service.configureTransformer("/weather") {
            try jsonDecoder.decode(DataModel.self, from: $0.content)
        }
        service.configureTransformer("/group") {
            try jsonDecoder.decode(getResults.self, from: $0.content).list
        }
    }
    // Single City
    func getWeatherData(for cityID: String) -> Resource {
        return service
            .resource("/weather")
            .withParam("id", cityID)
            .withParam("units","Metric")
            .withParam("APPID","096140cff5cad7aaa5b7e7230124d024")
    }
    // multiple cities //
    func getWeatherDataforMultipleCities(for cityIDs: String) -> Resource {
        return service
            .resource("/group")
            .withParam("id", cityIDs)
            .withParam("units","Metric")
            .withParam("APPID","096140cff5cad7aaa5b7e7230124d024")
    }
}
