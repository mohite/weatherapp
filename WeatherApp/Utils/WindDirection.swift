//
//  WindDirection.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 24/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import Foundation

enum WindDirection: String {
    case n, nne, ne, ene, e, ese, se, sse, s, ssw, sw, wsw, w, wnw, nw, nnw
}
extension Double {
    var direction: WindDirection {
        return WindDirection(self)
    }
}

extension WindDirection: CustomStringConvertible  {
    static let all: [WindDirection] = [.n, .nne, .ne, .ene, .e, .ese, .se, .sse, .s, .ssw, .sw, .wsw, .w, .wnw, .nw, .nnw]
    init(_ direction: Double) {
        let index = Int((direction + 11.25).truncatingRemainder(dividingBy: 360) / 22.5)
        self = WindDirection.all[index]
    }
    var description: String {
        return rawValue.uppercased()
    }
}
