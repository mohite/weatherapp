//
//  MyColors.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 24/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import UIKit

enum MyColors {
    case cellSelectionColor
}

extension MyColors {
    var value: UIColor {
        get {
            switch self {
            case .cellSelectionColor:
                return UIColor(red: 39.0/255.0, green: 97.0/255.0, blue: 125.0/255.0, alpha: 1.0)
            }
        }
    }
}
