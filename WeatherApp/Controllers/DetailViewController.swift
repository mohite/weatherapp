//
//  DetailViewController.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 20/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var cityTemperature: UILabel!
    @IBOutlet weak var cityMinTemp: UILabel!
    @IBOutlet weak var cityMaxTemp: UILabel!
    @IBOutlet weak var cityHumidity: UILabel!
    @IBOutlet weak var cityWeatherType: UILabel!
    @IBOutlet weak var cityWeatherDescription: UILabel!
    @IBOutlet weak var cityWeatherImage: UIImageView!
    @IBOutlet weak var dummyView: UIView!
    
    @objc func dismissController()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureView() {
        
        // Assuming temerature is coming in Kelvin as Unit is Metric
        if let weather:DataModel = weatherData {
            if let view = dummyView
            {
                view.isHidden = true
            }
            self.title = (weather.name + ", " + weather.sys.country).uppercased()
            if let label = currentTime {
                label.text = getStringFromUnixTimeStamp(dateStamp: weather.dt)
            }
            if let label = cityTemperature {
                label.text = String(format:"%.1f%@",weather.main.temp-273,"\u{00B0}")
                
            }
            if let label = cityMinTemp {
                
                label.text = String(format:"%.1f%@",weather.main.temp_min-273,"\u{00B0}")
            }
            if let label = cityMaxTemp {
                label.text = String(format:"%.1f%@",weather.main.temp_max-273,"\u{00B0}")
            }
            if let label = cityHumidity {
                label.text = String(format:"%.0f%@ humidity",weather.main.humidity,"%")
            }
            if let label = cityWeatherType {
                label.text = weather.weather[0].main
            }
            if let label = cityWeatherDescription {
                let direction  = WindDirection(weather.wind.deg as Double)
                label.text = String(format:"%@ %.1f%@",direction.description,(18/5)*weather.wind.speed," Km/h")
            }
            if let imageView = cityWeatherImage {
                imageView.alpha = 0
                imageView.image = UIImage(named: weather.weather[0].icon)
                UIView.animate(withDuration: 2.0, delay: 0.5, options: [.curveEaseIn], animations: {
                    imageView.alpha = 1
                }, completion: nil)
                
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
        let rightButton: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissController))
        self.navigationItem.rightBarButtonItem = rightButton
    }

    var weatherData: DataModel? {
        didSet {
            // Update the view.
            configureView()
        }
    }
}
