//
//  MasterViewController.swift
//  WeatherApp
//
//  Created by Mohit Sharma on 20/2/19.
//  Copyright © 2019 Mohit Sharma. All rights reserved.
//

import UIKit
import CoreFoundation
import Siesta

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var dataObjects = [Dictionary<String, Any>]()
    let activityIndicator = ActivityIndicator.shared
    private var statusOverlay = ResourceStatusOverlay()
    
    private var weatherData: [DataModel] = [] {
        didSet {
            tableView.reloadData()
            self.activityIndicator.stopAnimating(navigationItem: self.navigationItem)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        getDataUsingWeatherAPI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
           if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

                controller.weatherData = weatherData[indexPath.row]
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let data:DataModel = weatherData[indexPath.row]
        cell.textLabel?.text = data.name
        // as unit is metric assuming temperature is in Kelvin
        
        cell.detailTextLabel?.text = String(format:"%.1f%@",data.main.temp-273,"\u{00B0}")
        let bgColorView = UIView()
        bgColorView.backgroundColor = MyColors.cellSelectionColor.value
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
}

// Siesta methods and callbacks
extension MasterViewController: ResourceObserver {
    func getDataUsingWeatherAPI()
    {
        activityIndicator.animateActivity(title: "Loading...", view: self.view, navigationItem: navigationItem)
            WeatherAPI.sharedInstance.getWeatherDataforMultipleCities(for: "2147714,2158177,2174003")
                .addObserver(self)
                .addObserver(statusOverlay, owner: self)
                .loadIfNeeded()
        statusOverlay.embed(in: self)
    }
    func resourceChanged(_ resource: Resource, event: ResourceEvent) {
        //NSLog("Hello")
        weatherData = resource.typedContent() ?? []
    }
}

